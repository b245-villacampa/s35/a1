const express = require("express");
// Mongoose is a package that allows us to create Schemas to model our structure and to manipulate our data using different access methods.
const mongoose = require("mongoose");

const port = 3001;

const app = express();

	// [SECTION]MongoDB Connection
		// Syntax:
			// mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection})

	mongoose.connect("mongodb+srv://admin:admin@batch245-villacampa.g4r4ic2.mongodb.net/s35-discussion?retryWrites=true&w=majority", {

		// Allows us to avoid any current and future errors while connecting to mongodb
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

	let db = mongoose.connection;

	// error handling in connection
	db.on("error", console.error.bind(console,"connection error"))

	// this will be triggered if the connection is succesful 
	db.once("open", ()=> console.log("We're connected to the cloud database!"));

	// Mongoose Schemas
		// Schemas determined the structure of the documents to be written in the database.
		// Schemas act as the blueprint to our data
		//Syntax
			//const schemaName = new mongoose.Schema({keyvaluepairs})

	// taskSchema it contains two properties: name & status
	// required is used to specify taht a field must not be empty.
	// default - is used if a field value is not supplied.
	const taskSchema = new mongoose.Schema({
		name:{
			type:String,
			required:[true, "Task name is requires!"]
		},
		status:{
			type:String,
			default: "pending"
		}
	})

	// Start of activity Schema

		const userSchema = new mongoose.Schema({
			username:{
				type:String,
				required:[true, "Username is required!"]
			},
			password:{
				type:String,
				required:[true, "Password is required!"]
			}
		})

	// End of Activity Schema

	// {SECTION} Models
		// Uses schema to create/instatiate documents/objects taht follow our schema structure

		// the variable/object that will be created can be used to run commands with our database

		// Synatx:
			// const variableName = mongoose.model("collectionName", schemaName);

		const Task = mongoose.model("Task", taskSchema)

		// Start of the activity model
		const User = mongoose.model("User", userSchema)
		// End of the activity model
		
	// Middlewares
		app.use(express.json()) //Allows the app to read json data
		app.use(express.urlencoded({extended: true})) //it will allow our app to read data from forms.

	// [SECTION] Routing
		// Create/ add new task
			//1.Check if the task is existing
				//if task already exist in the databse, we will return a message "The task is already existing!" 
				//if the task doesnt exist in the db, we will add it in the db

		app.post("/tasks", (request, response)=>{
			let input = request.body;

			console.log(input.status);
			if(input.status === undefined){
				Task.findOne({name:input.name}, (error, result) => {console.log(result);

					if(result != null){
						return response.send("The task is already existing!")
					} else{
						let newTask = new Task({
							name: input.name
						})
						
						// save() method will save the object in the collection that the object instiated
						newTask.save((saveError, savedTask)=>{
							if(saveError){
								return console.log(saveError)
							}else{
								return response.send("New Task created!")
							}
						})

					}
				})
			}
			else{
				Task.findOne({name:input.name}, (error, result) => {console.log(result);

				if(result != null){
					return response.send("The task is already existing!")
				} else{
					let newTask = new Task({
						name: input.name,
						status:input.status
					})
					
					// save() method will save the object in the collection that the object instiated
					newTask.save((saveError, savedTask)=>{
						if(saveError){
							return console.log(saveError)
						}else{
							return response.send("New Task created!")
						}
					})

				}
			})
			}
		})

		// [SECTION] Retrieving all the tasks
			app.get("/tasks", (request, response)=>{
				Task.find({}, (error, result) => {
					if(error){
						console.log(error);
					}else{
						return response.send(result);
					}
				})
			});

		// Start of activity route
		
		app.post("/signup", (request, response)=>{
			let userInput = request.body;
			User.findOne({username:userInput.username}, (error, result)=> {
					
					if(result != null){
						return response.send("The Username already exist!")
					}else{
						let newUser = new User({
							username: userInput.username,
							password: userInput.password
						})

						newUser.save((saveError, saveUser)=>{
							if(saveError){
								return console.log(saveError)
							}else{
								return response.send("New User Created!");
							}
						})
					}
				})
		});

		// End of activity route

	app.listen(port, ()=>console.log(`The Server is running at port: ${port}!`))

	